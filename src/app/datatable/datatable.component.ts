import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl,Validators } from '@angular/forms';
import { data } from 'jquery';
import { Subject } from 'rxjs';
import { first } from 'rxjs/operators';
import { ApiServiceService } from '../api-service.service';
@Component({
  selector: 'app-datatable-table',
  templateUrl: './datatable.component.html',
  styleUrls: ['./datatable.component.css']
})
export class DataTableComponent implements OnInit {
  getAllDatas: any=[];
  manDetails: any;
  editClick:boolean=true;
  product: any;
  updateClick: boolean=true;
  mannameEdit: any={};
  descnameEdit: any={};
  constructor(private api:ApiServiceService,private http: HttpClient) { }
  dataSource:[] | any;
  dtOptions:DataTables.Settings={};
  dtTrigger: Subject<any> = new Subject<any>();
  dataForm!: FormGroup;
  alert: boolean = false;
  // deleteAlert:boolean=false;
  currentId!:number;
  updateData:[] | any;
  isupdate:boolean=false;


  // constructor(private http: HttpClient) { }

  ngOnInit(): void {
    this.dtOptions={
      pagingType:'full_numbers',
      pageLength: 5,
      lengthMenu: [5,10,15,25],
      processing: true
  }


  this.dataForm = new FormGroup({
    manufacturerName: new FormControl('',[ Validators.required,Validators.minLength(3)]),
    description: new FormControl('')
  });

  // console.log("HHHHiiiiiiiii")
  this.getAllData()

}

getAllData(){
  
  this.http.post('http://159.65.151.134/api/ManufacturerAPI/GetAll',"p")
  .subscribe((result: any)=> {
    console.log(result);
    this.getAllDatas = result
   
     console.log(this.getAllDatas);
    this.dtTrigger.next();
  
    console.warn("submitted",result);
  })
 
}

onSubmit(data: any) {
  console.warn(data)
      this.http.post('http://159.65.151.134/api/ManufacturerAPI/Post',data)
    .subscribe((result: any)=> {
         this.getAllData()
         
     console.warn("submitted",result);
 
    //  window.location.reload();
     })
   }
   updateProduct(product:any) {
    console.log(product);
    this.getAllData()
    // this.mannameEdit = this.manDetails.manufacturerName
    // this.descnameEdit = this.manDetails.description
    // this.updateClick = false
    // window.location.reload();
   }
   editProduct(product:any) {
    console.log(product);
    this.manDetails = product
    this.ngOnInit()
    this.editClick = false
  }
  onEdit()
  {
  console.log(this.manDetails)
   this.http.put('http://159.65.151.134/api/ManufacturerAPI/Put/' + this.manDetails.manufacturerId,this.manDetails)
    .subscribe((result: any)=>{
      this.editClick = true
      this.getAllData() 
     
      console.warn("submitted",result);
      // window.location.reload();
  });
  }

//  get manufacturerName() {
//   return this.dataForm?.get('manufacturerName');
//  }






  

deleteItem(product:'manufacturerId'){
//  console.log(product)
    this.http.delete('http://159.65.151.134/api/ManufacturerAPI/Delete/' +product)
    .subscribe((result: any)=>{
      this.getAllData()
    console.warn("submitted",result);
    //  window.location.reload();
})
}

}

